package com.ems.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ems.entities.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer>{
	
	User findUserById(int userId);
	
	User findByName(String name);
	
	User findByRole(String role);
	
	User findByEmail(String email);

	

	User findByDepartment(String dept);
	
	
	

}
