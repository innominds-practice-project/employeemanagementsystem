package com.ems.entities;



import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "address")
public class Address {



@GeneratedValue(strategy = GenerationType.IDENTITY)
@Id
@Column(name = "address_id")
private int id;
@Column(name = "landmark")
private String landmark;
@Column(name = "city")
private String city;
@Column(name = "country")
private String country;
@Column(name = "pin")
private String pin;


@ManyToOne(cascade = CascadeType.ALL)
@JoinColumn(name = "user_id")
private User user;




public Address() {
	super();
	// TODO Auto-generated constructor stub
}



public Address(int id, String landmark, String city, String country, String pin, User user) {
super();
this.id = id;
this.landmark = landmark;
this.city = city;
this.country = country;
this.pin = pin;
this.user = user;
}



public int getId() {
return id;
}



public void setId(int id) {
this.id = id;
}



public String getLandmark() {
return landmark;
}



public void setLandmark(String landmark) {
this.landmark = landmark;
}



public String getCity() {
return city;
}



public void setCity(String city) {
this.city = city;
}



public String getCountry() {
return country;
}



public void setCountry(String country) {
this.country = country;
}



public String getPin() {
return pin;
}



public void setPin(String pin) {
this.pin = pin;
}



public User getUser() {
return user;
}



public void setUser(User user) {
this.user = user;
}



@Override
public String toString() {
	return "Address [id=" + id + ", landmark=" + landmark + ", city=" + city + ", country=" + country + ", pin=" + pin
			+ "]";
}





}