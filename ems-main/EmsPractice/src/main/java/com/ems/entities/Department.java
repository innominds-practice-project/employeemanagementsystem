package com.ems.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "department")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "department_id")
	private int id;
	@Column(name = "department_name", unique = true)
	private String departmentName;
	
	@OneToMany(mappedBy = "department", fetch = FetchType.LAZY)
	private List<User> employeeList;

	
	
	
	public Department() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Department(int id, String departmentName, List<User> employeeList) {
		super();
		this.id = id;
		this.departmentName = departmentName;
		this.employeeList = employeeList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public List<User> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<User> employeeList) {
		this.employeeList = employeeList;
	}

	@Override
	public String toString() {
		return "Department [departmentID=" + id + ", departmentName=" + departmentName + ", employeeList="
				+ employeeList + "]";
	}

}
