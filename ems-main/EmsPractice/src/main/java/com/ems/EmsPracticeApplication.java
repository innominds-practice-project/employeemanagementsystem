package com.ems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmsPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmsPracticeApplication.class, args);
		System.out.println("spring practice project");
	}

}
