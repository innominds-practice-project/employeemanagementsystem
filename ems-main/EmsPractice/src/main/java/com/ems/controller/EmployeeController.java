package com.ems.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.ems.daos.UserDao;
import com.ems.entities.User;

@RestController
public class EmployeeController {
@Autowired
	private UserDao repository;

@PostMapping("/saveEmployee")
public String saveEmployee(@RequestBody User emp) {
	repository.save(emp);
	return "Employee Saved";
}
@GetMapping("/getAllEmployee")
public List<User> getAllEmployee(){
	return  repository.findAll();
}
@GetMapping("/getEmployee/{id}")
public Optional<User> getEmployee(@PathVariable("id") int id) {
	return repository.findById(id);
}

@DeleteMapping("deleteEmployeeById/{id}")
public String deleteEmployee(@PathVariable("id") int id){
	repository.deleteById(id);
	return "Employee Deleted successfully";
}

@GetMapping("/getEmployeeByDept/{dept}")
public User getEmployeeByDept(@PathVariable("dept") String dept) {
	return repository.findByDepartment(dept);
	
}



}
